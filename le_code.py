import nltk
from nltk.stem.lancaster import LancasterStemmer
ls = LancasterStemmer()

import numpy
import tflearn
import tensorflow
#from tensorflow.python.compiler.tensorrt import trt_convert as trt
import random
import json
import pickle

#reading data
with open("intents.json") as file:
    data = json.load(file)

try: #access saved data, doesn't generate it again if unecessary
    with open("data.pickle", "rb") as f:
        words, labels, training, output = pickle.load(f)
except:
    words = []
    labels = []
    docs_x = [] #for pattern
    docs_y = [] #for tag

#looking up in the file, also adding new lines
    for intent in data["intents"]:
        for pattern in intent["patterns"]:
            words2 = nltk.word_tokenize(pattern)
            words.extend(words2)
            docs_x.append(words2)
            docs_y.append(intent["tag"])
    
            if intent["tag"] not in labels:
                labels.append(intent["tag"])
    
    words = [ls.stem(w.lower()) for w in words if w != "?"]
    words = sorted(list(set(words)))
    
    labels = sorted(labels)

#training the model
    training = []
    output = []
    
    out_empty = [0 for _ in range(len(labels))]
#creating the bag words
    for x,doc in enumerate(docs_x):
        bag = []
    
        words2 = [ls.stem(w) for w in doc]
    
        for w in words:
            if w in words2:
                bag.append(1)
            else:
                bag.append(0)
    
            output_row = out_empty[:]
            output_row[labels.index(docs_y[x])] = 1
    
            training.append(bag)
            output.append(output_row)
    
    training = numpy.array(training)
    output = numpy.array(output)
#building model
    #writing/saving these variables in a pickle file, so the code doesn't generate new ones every single time if unecessary
    with open("data.pickle", "wb") as f:
        pickle.dump((words, labels, training, output),f) 
        
try: 
    model.load("model.tflearn") #again, verifying if generating data is necessary
except:
    tensorflow.reset_default_graph()

    net = tflearn.input_data(shape=[None, len(training[0])])
    net = tflearn.fully_connected(net, 8)  # hidden layer of 8 neurons
    net = tflearn.fully_connected(net, 8)  # yup, another layer
    net = tflearn.fully_connected(net, len(output[0]), activation="softmax")  # output layer, probabilities
    net = tflearn.regression(net)

    model = tflearn.DNN(net)

    model.fit(training, output, n_epoch = 1000, batch_size = 8, show_metric = True) #n_epoch - amount of time in which model sees the same data
    model.save("model.tflearn")
    
def list_of_words(sentence, words): #getting the words from the input sentence
    bag = [0 for _ in range(len(words))]
    
    sen_words = nltk.word_tokenize(sentence)
    sen_words = [ls.stem(word.lower()) for word in sen_words]
    
    for se in sen_words:
        for i, w in enumerate(words):
            if w == se:
                bag[i] = 1
                
    return numpy.array(bag)

def le_chat():
    print("Chat with the bot, don't be shy! (type 'quit' to stop)")
    while True:
        inp = input("You: ")
        if inp.lower() == "quit":
            break
            
        results = model.predict([list_of_words(inp, words)])
        results_index = numpy.argmax(results) #index of the greatest value of probabilities of those neurons
        tag = labels[results_index]
        
        if results[results_index] > 0.7: #'70% bot confidence' or higher, probability of the line to be suitable
            for tg in data["intents"]:
                if tg['tag'] == tag:
                    responses = tg['responses']
                    
            print(random.choice(responses))
        else: 
            print("I'm sorry I'm dumb, would you mind reprashing?")
        
le_chat()